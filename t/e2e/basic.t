use Test::More;
use Test::Mojo;

Test::Mojo->new->get_ok( 'http://server:8080/tested' )
    ->status_is(200)
    ->content_is('tested');

done_testing();

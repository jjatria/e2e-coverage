FROM perl:5.30 AS base

COPY cpanfile          ./cpanfile
COPY cpanfile.snapshot ./cpanfile.snapshot

RUN cpanm -nq --installdeps .

RUN ln -s /root /project
WORKDIR /project

FROM base AS e2e

RUN cpanm -nq --installdeps --with-feature e2e .

COPY t ./t

CMD [ "prove", "-lr", "t/e2e" ]

FROM base AS dev

RUN cpanm -nq --installdeps --with-feature dev .

CMD [ "morbo", "-l", "http://*:8080", "server" ]

FROM base

COPY server ./server

CMD [ "hypnotoad", "-f", "server" ]

requires 'Mojolicious' => '== 8.23';

feature e2e => 'Coverage for end-to-end tests' => sub {
    requires 'Devel::Cover' => '== 1.33';
    requires 'Sereal'       => '== 4.007';
};

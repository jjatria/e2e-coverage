Run the tests with:

    docker-compose -f docker-compose/e2e.yml up \
        --exit-code-from e2e --abort-on-container-exit --build

If your system has Devel::Cover and Sereal (or whatever serialisation
module you used to gather the coverage data), you can generate the report
with

    cover

Otherwise, you can generate it within the container itself

    docker-compose -f docker-compose/e2e.yml run server cover
